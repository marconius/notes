Workstation
==========


Instructions
-----

### Roles

#### Option 1 - install roles

Galaxy install ... TODO

#### Option 2 - clone roles

Set ansible role directory ... clone


### Run playbook

#### Vars

See galaxy roles in `requirements.yml` for role vars.

##### Playbook vars

* `sysctl_inotify_max_user_watches` defaults to `'524288'`

```
$ sudo apt-get install python3-dev python3-venv libssl-dev
$ python3 -m venv /path/to/new/virtual/environment
$ . /path/to/new/virtual/environment/bin/activate
$ pip install -r requirements.txt
$ ansible-galaxy install -r requirements.yml
$ ansible-playbook workstation.yml -K -i hosts
```

